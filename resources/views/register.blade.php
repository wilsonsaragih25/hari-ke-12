<!DOCTYPE html>
<html>
<head>
    <title>Register</title>
</head>
<body>
    <h1>Register</h1>
    <form action="{{ route('welcome') }}" method="post">
        @csrf
        <label for="first_name">First Name:</label>
        <input type="text" name="first_name" required><br>

        <label for="last_name">Last Name:</label>
        <input type="text" name="last_name" required><br>

        <label for="gender">Gender:</label>
        <input type="checkbox" name="gender" value="male"> Male
        <input type="checkbox" name="gender" value="female"> Female<br>

        <label for="nationality">Nationality:</label>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="usa">USA</option>
            <option value="uk">UK</option>
            <!-- Add other nationality options as needed -->
        </select><br>

        <label for="languages_spoken">Languages Spoken:</label><br>
        <input type="checkbox" name="languages_spoken[]" value="indonesian"> Indonesian<br>
        <input type="checkbox" name="languages_spoken[]" value="english"> English<br>
        <input type="checkbox" name="languages_spoken[]" value="other"> Other<br>

        <label for="bio">Bio:</label>
        <textarea name="bio" rows="4" cols="50"></textarea><br>

        <button type="submit">Register</button>
    </form>
</body>
</html>
